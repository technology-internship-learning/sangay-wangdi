# Electronic Designing

![](./Images/electronics-commons.jpg)

## First Learning Experience
During our first learning experience I learned about micro controller which is like the brain of the circuit which controls and helps in the functioning of the whole circuit. We learned about the attiny44 which is a micro contoller and used kicad to design a footprint for our circuit board. 

## Second and Third Learning Experience
We learned to solder certain components such as capacitors, resistors and buttons on to the circuit board. Though we couldn't use the SAM-20 machine, we learned in theory how to operate it and what the circuit board is made of. Then we learned about what materials are used during soldering. 


